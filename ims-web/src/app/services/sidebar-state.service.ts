import {Injectable} from "@angular/core";
import {Logger} from "./logger.service";

@Injectable()
export class SidebarStateService {

  private sidebarOverlayMode: boolean = false;
  private sidebarStateOpen: boolean = true;
  private sidebarStateExpanded:boolean = true;

  getSidebarOverlayMode(): boolean {
    return this.sidebarOverlayMode;
  }

  setSidebarOverlayMode(value: boolean) {
    this.sidebarOverlayMode = value;
    Logger.log('overlay: ' + String(this.sidebarOverlayMode));
  }

  getSidebarStateOpen(): boolean {
    return this.sidebarStateOpen;
  }

  setSidebarStateOpen(value: boolean) {
    this.sidebarStateOpen = value;
    Logger.log('open: ' + String(this.sidebarStateOpen));
  }

  getSidebarStateExpanded(): boolean {
    return this.sidebarStateExpanded;
  }

  setSidebarStateExpanded(value: boolean) {
    this.sidebarStateExpanded = value;
    Logger.log('expanded: ' + String(this.sidebarStateExpanded));
  }

}
