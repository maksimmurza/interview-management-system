import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Candidate} from '../components/main-area/candidates/candidate/candidate.model';
import {Logger} from './logger.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class CandidatesService {

  private baseUrl = 'http://localhost:3000';
  private candidatesUrl = '/candidates';
  private selectedCandidate: Candidate;
  private serverState: boolean;

  constructor(private http: Http) {}

  getSelectedCandidate(): Candidate {
    return this.selectedCandidate;
  }

  setSelectedCandidate(value: Candidate) {
    this.selectedCandidate = value;
  }

  getServerState(): boolean {
    return this.serverState;
  }

  setServerState(value: boolean) {
    this.serverState = value;
  }

  getCandidates(page: number): Promise<Array<Candidate>> {
    return this.http
      .get(this.baseUrl + this.candidatesUrl + '?_page=' + page + '&_limit=30')
      .toPromise()
      .then((response) => {
        Logger.log(response.json());
        this.serverState = true;
        return response.json() as Candidate[];
      }, reason => {Logger.log('Error'); this.serverState = false;})
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
