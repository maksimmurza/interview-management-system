import { NgModule } from '@angular/core';
import { CandidatesModule } from './components/main-area/candidates/candidates.module';
import { HttpModule, JsonpModule } from "@angular/http";
import { AppComponent } from './app.component';
import { Logger } from './services/logger.service';
import { BrowserModule } from "@angular/platform-browser";
import { CommonModule } from "@angular/common";
import {NavbarComponent} from "./components/main-area/navbar/navbar.component";
import {NavbarModule} from "./components/main-area/navbar/navbar.module";
import {MainAreaComponent} from "./components/main-area/main-area.component";
import {MainAreaModule} from "./components/main-area/main-area.module";
import {SidebarModule} from "./components/sidebar/sidebar.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MainAreaModule,
    SidebarModule,
    HttpModule,
    JsonpModule
  ],
  providers: [Logger],
  bootstrap: [AppComponent]
})
export class AppModule { }
