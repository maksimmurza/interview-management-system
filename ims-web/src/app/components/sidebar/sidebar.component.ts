import { Component, OnInit } from '@angular/core';
import {Logger} from "../../services/logger.service";
import {SidebarStateService} from "../../services/sidebar-state.service";

@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {

  ngOnInit(): void {
    if(window.innerWidth < 992) {
      Logger.log(window.innerWidth.toString());
      this.sidebarStateService.setSidebarStateOpen(false);
      this.sidebarStateService.setSidebarOverlayMode(true);
    }
  }

  constructor(public sidebarStateService: SidebarStateService) {

  }

  toggleSidebar() {
    this.sidebarStateService.getSidebarStateExpanded() ?
      this.sidebarStateService.setSidebarStateExpanded(false) :
      this.sidebarStateService.setSidebarStateExpanded(true);
    Logger.log(String(this.sidebarStateService.getSidebarStateExpanded));
  }
}


