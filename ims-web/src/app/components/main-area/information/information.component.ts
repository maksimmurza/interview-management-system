import {Component, Input, OnInit} from '@angular/core';
import {Candidate} from "../candidates/candidate/candidate.model";
import {CandidatesService} from "../../../services/candidates.servise";
import {InformationService} from "../../../services/information.service";
import {Information} from "./information.model";
import {Logger} from "../../../services/logger.service";
import {$} from "protractor";
import {CandidatesComponent} from "../candidates/candidates.component";
import {SidebarStateService} from "../../../services/sidebar-state.service";

@Component({
  selector: 'information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit{

  @Input() candidate: Candidate;
  activeTab = 1;

  ngOnInit(): void {
  }

  constructor(private candidatesService: CandidatesService,
              private informationService: InformationService,
              private sidebarStateService: SidebarStateService) {}

  selectTab(tab: number) {
    this.activeTab = tab;
  }

  getReview() {
    return this.informationService.getReview();
  }

  getServerState() {
    return this.candidatesService.getServerState();
  }

  getDisplayProperty(): boolean {
    if(!this.sidebarStateService.getSidebarOverlayMode()) {
      return true;
    } else {
      if(this.informationService.getVisible()) {
        return true;
      } else return false;
    }
  }

}
