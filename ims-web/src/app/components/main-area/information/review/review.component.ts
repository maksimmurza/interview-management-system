import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Candidate} from "../../candidates/candidate/candidate.model";
import {CandidatesService} from "../../../../services/candidates.servise";
import {InformationService} from "../../../../services/information.service";
import {Information, InterviewerComment} from "../information.model";
import {Logger} from "../../../../services/logger.service";

@Component({
  selector: 'review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent {

  @Input() review: Information;

  constructor(private candidatesService: CandidatesService) {}

}
