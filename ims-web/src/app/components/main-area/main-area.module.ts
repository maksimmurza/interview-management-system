import { NgModule } from '@angular/core';
import {BrowserModule} from "@angular/platform-browser";
import {Logger} from "../../services/logger.service";
import {MainAreaComponent} from "./main-area.component";
import {NavbarModule} from "./navbar/navbar.module";
import {CandidatesModule} from "./candidates/candidates.module";
import {FooterComponent} from "./footer/footer.component";
import {SidebarModule} from "../sidebar/sidebar.module";
import {SidebarStateService} from "../../services/sidebar-state.service";
import {InformationModule} from "./information/information.module";
import {OverlayComponent} from "./overlay/overlay.component";
import {LinksComponent} from "./links/links.component";

@NgModule({
  declarations: [
    MainAreaComponent,
    FooterComponent,
    OverlayComponent,
    LinksComponent
  ],
  imports: [
    BrowserModule,
    CandidatesModule,
    SidebarModule,
    InformationModule,
    NavbarModule
  ],
  providers: [
    Logger,
    SidebarStateService
  ],
  exports: [
    MainAreaComponent
  ],
})
export class MainAreaModule { }
