import {Component, Input} from '@angular/core';
import {Candidate} from './candidate.model';
import {strictEqual} from "assert";
import {InformationService} from "../../../../services/information.service";

@Component({
  selector: 'candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.css']
})
export class CandidateComponent {
  @Input() candidate: Candidate;

  constructor(private informationService: InformationService) { }

  ngOnInit() {

  }
}
