import { Component, OnInit } from '@angular/core';
import { Candidate } from "./candidate/candidate.model";
import { CandidatesService } from "../../../services/candidates.servise"
import {Logger} from "../../../services/logger.service";
import {ReviewComponent} from "../information/review/review.component";
import {InformationService} from "../../../services/information.service";
import {SidebarStateService} from "../../../services/sidebar-state.service";


@Component({
  selector: 'candidates',
  templateUrl: './candidates.component.html',
  styleUrls: ['./candidates.component.css']
})
export class CandidatesComponent implements OnInit {

  candidates: Candidate[];
  loadedData: Candidate[];
  error: any;
  page = 1;

  constructor(private candidatesService: CandidatesService,
              private informationService: InformationService,
              private sidebarStateService: SidebarStateService) {

  }

  ngOnInit() {
    this.getCandidates();
  }

  getCandidates(): void {
    this.candidatesService.getCandidates(this.page)
      .then(candidates => {
        this.page > 1 ?
          this.candidates = this.candidates.concat(candidates) :
          this.candidates = candidates;
        this.page++;
      }, reason => {Logger.log('Error');})
      .catch(error => this.error = error);
  }

  selectCandidate(candidate: Candidate) {
    this.candidatesService.setSelectedCandidate(candidate);
    this.informationService.updateReview();
  }

  setInformationVisible() {
    if(this.sidebarStateService.getSidebarOverlayMode()) {
      this.informationService.setVisible(true);
    }
  }

  getDisplayProperty() {
    if(!this.sidebarStateService.getSidebarOverlayMode()) {
      return true;
    } else {
      if(!this.informationService.getVisible()) {
        return true;
      } else return false;
    }
  }
}

