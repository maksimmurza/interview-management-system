import {Component, Input, OnInit} from '@angular/core';
import {SidebarStateService} from "../../services/sidebar-state.service";
import {Candidate} from "./candidates/candidate/candidate.model";
import {CandidatesService} from "../../services/candidates.servise";
import {Information} from "./information/information.model";
import {InformationService} from "../../services/information.service";
import {Logger} from "../../services/logger.service";

@Component({
  selector: 'main-area',
  templateUrl: './main-area.component.html',
  styleUrls: ['./main-area.component.css']
})
export class MainAreaComponent {

  constructor(public sidebarStateService: SidebarStateService,
              private candidatesService: CandidatesService,
              private informationService: InformationService) {
  }

  onResize(event) {
    /*When width decrease*/
    if(event.target.innerWidth < 992 && this.sidebarStateService.getSidebarStateOpen() &&
                                        !this.sidebarStateService.getSidebarOverlayMode()) {
      this.sidebarStateService.setSidebarStateOpen(false);
      this.sidebarStateService.setSidebarOverlayMode(true);   }
    /*When width increase*/
    if(event.target.innerWidth > 991 && this.sidebarStateService.getSidebarOverlayMode()) {
      this.sidebarStateService.setSidebarStateOpen(true);
      this.sidebarStateService.setSidebarOverlayMode(false);
      this.sidebarStateService.setSidebarStateExpanded(true);
      this.informationService.setVisible(false);
    }
  }

  getInformation(): Candidate {
    return this.candidatesService.getSelectedCandidate();
  }

}
