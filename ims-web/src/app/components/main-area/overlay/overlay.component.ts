import { Component, OnInit } from '@angular/core';
import {SidebarStateService} from "../../../services/sidebar-state.service";

@Component({
  selector: 'overlay',
  templateUrl: './overlay.component.html',
  styleUrls: ['./overlay.component.css']
})
export class OverlayComponent {

  constructor(public sidebarStateService: SidebarStateService) {
  }

}
