module.exports = function() {
	var faker = require("faker");
	var _ = require("lodash");
	return {
		candidates: _.times(10000, function (n) {
			return {
				id: n,
				name: faker.name.findName(),
				flag: faker.random.arrayElement([0, 1, 2, 3]),

				badges: _.times(faker.random.number({'min' : 1, 'max' : 4}), function () {
					return faker.random.arrayElement(["Army","Student","SQL","Interesting", "Linux", "Java"])}),
				
				description: faker.lorem.paragraph(),
				avatar: faker.internet.avatar()
			}
		}),

        information: _.times(10000, function (n) {
            return {
                id: n,
                comments: _.times(faker.random.number({'min' : 3, 'max' : 10}), function () {
                    return {
                    	interviewer: faker.random.arrayElement(["Siarhei Berdachuk","Aliaksandr Fedasiuk"]),
						date: faker.date.past(),
                        comment: faker.lorem.paragraph()
					}
                })
            }
        })
	}
};


