import { ImsWebPage } from './app.po';

describe('ims-web App', () => {
  let page: ImsWebPage;

  beforeEach(() => {
    page = new ImsWebPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
