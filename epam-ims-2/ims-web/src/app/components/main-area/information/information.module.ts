import { NgModule } from '@angular/core';
import {BrowserModule} from "@angular/platform-browser";
import {Logger} from "../../../services/logger.service";
import {InformationComponent} from "./information.component";
import {CandidatesService} from "../../../services/candidates.servise";
import {InformationService} from "../../../services/information.service";
import {ReviewComponent} from "./review/review.component";

@NgModule({
  declarations: [
    InformationComponent,
    ReviewComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    Logger,
    CandidatesService,
    InformationService
  ],
  exports: [
    InformationComponent,
    ReviewComponent
  ],
})
export class InformationModule { }
