export class Information {
  id: number;
  comments: InterviewerComment[];
}

export class InterviewerComment {
  interviewer: string;
  date: string;
  comment: string;
}
