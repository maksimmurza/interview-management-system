import { Component, OnInit } from '@angular/core';
import {SidebarStateService} from "../../../services/sidebar-state.service";
import {OverlayComponent} from "../overlay/overlay.component";
import {InformationService} from "../../../services/information.service";

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  constructor(public sidebarStateService: SidebarStateService,
              public informationService: InformationService) {

  }

  setInformationUnvisible() {
    if(this.sidebarStateService.getSidebarOverlayMode()) {
      this.informationService.setVisible(false);
    }
  }

}
