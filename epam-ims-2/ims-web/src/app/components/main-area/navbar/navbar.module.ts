import { NgModule } from '@angular/core';
import {NavbarComponent} from "./navbar.component";
import {BrowserModule} from "@angular/platform-browser";
import {Logger} from "../../../services/logger.service";
import {SidebarStateService} from "../../../services/sidebar-state.service";


@NgModule({
  declarations: [
    NavbarComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    Logger,
    SidebarStateService
  ],
  exports: [
    NavbarComponent
  ],
})
export class NavbarModule { }
