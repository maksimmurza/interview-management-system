export class Candidate {
  id: number;
  name: string;
  flag: number;
  badges: string[];
  description: string;
  avatar: string;
}
