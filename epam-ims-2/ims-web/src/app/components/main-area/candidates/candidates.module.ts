import { NgModule } from '@angular/core';
import { CandidatesComponent } from './candidates.component';
import { CandidateComponent } from './candidate/candidate.component';
import { HttpModule, JsonpModule } from '@angular/http';
import { CandidatesService } from '../../../services/candidates.servise';
import { Logger } from "../../../services/logger.service";
import { BrowserModule } from "@angular/platform-browser";
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import {InformationService} from "../../../services/information.service";

@NgModule({
  declarations: [
    CandidatesComponent,
    CandidateComponent
  ],
  imports: [
    HttpModule,
    JsonpModule,
    BrowserModule,
    InfiniteScrollModule
  ],
  providers: [
    CandidatesService,
    InformationService,
    Logger
  ],
  exports: [
    CandidatesComponent
  ],
})
export class CandidatesModule { }
