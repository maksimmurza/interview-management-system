import { NgModule } from '@angular/core';
import { BrowserModule } from "@angular/platform-browser";
import { Logger } from "../../services/logger.service";
import {SidebarComponent} from "./sidebar.component";
import {SidebarStateService} from "../../services/sidebar-state.service";


@NgModule({
  declarations: [
    SidebarComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    Logger,
    SidebarStateService
  ],
  exports: [
    SidebarComponent
  ],
})
export class SidebarModule { }
