import {Injectable} from "@angular/core";

@Injectable()
export class Logger {
  static log(message: string) { console.log(message); }
}
