import {Injectable} from "@angular/core";
import {Information} from "../components/main-area/information/information.model";
import {Http} from '@angular/http';
import {Logger} from "./logger.service";
import {Candidate} from "../components/main-area/candidates/candidate/candidate.model";
import {CandidatesService} from "./candidates.servise";

@Injectable()
export class InformationService {

  private baseUrl = 'http://localhost:3000';
  private informationUrl = '/information';
  private review: Information;
  private visible: boolean;
  error: any;

  constructor(private http: Http, private candidatesService: CandidatesService) {}

  setVisible(value: boolean) {
    this.visible = value;
  }

  getVisible() {
    return this.visible;
  }

  getReview() {
    return this.review;
  }

  getFlagColor(flag: number): string {
    switch (flag) {
      case 1: return 'green';
      case 2: return 'yellow';
      case 3: return 'red';
    }
  }

  getBadgeClass(badge: string): string {
    let skills = "SQL Linux Java Spring Angular js";
    let position = "Army Student";
    let good_opinion = "Interesting";
    let bad_opinion = "Strange";

    if(skills.includes(badge))
      return "badge badge-primary";
    else if (position.includes(badge))
      return "badge badge-warning";
    else if(good_opinion.includes(badge))
      return "badge badge-success";
    else if(bad_opinion.includes(badge))
      return "badge badge-danger";
  }

  reviewRequest(candidateId: number): Promise<Information[]> {
    return this.http
      .get(this.baseUrl + this.informationUrl + '?id=' + candidateId)
      .toPromise()
      .then((response) => {
        Logger.log(response.json());
        return response.json() as Information[];
      }, reason => {Logger.log("Сервис получения описания не отработал:" + reason)})
      .catch(this.handleError);
  }

  updateReview(): void {
    this.reviewRequest(this.candidatesService.getSelectedCandidate().id)
      .then(information => {Logger.log("Обратились к сервису и получили ответ"); this.review = information[0];
          Logger.log("id полученного объекта: " + this.review.id.toString());},
        reason => {Logger.log("Сервис вернул причину сбоя:" + reason);}
      ).catch(error => this.error = error);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
