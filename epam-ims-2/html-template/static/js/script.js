function openNav() {

    $("#sidebar-toggle-button").attr("class","fa fa-angle-double-left");
    $("#hide-btn").attr("onclick","closeNav()");
    $("#menu").attr("data-target","#menu1");

    document.getElementById("mySidenav").style.width = "300px";
    document.getElementById("main").style.marginLeft = "300px";

    toggleTextVisibility("menu-text", "inline");
    document.getElementById("hide-item").style.display = "inline";
}

function closeNav() {

    if($("#mySidenav").hasClass("sidenav-expanded-lg")) {
        $("#mySidenav").toggleClass('sidenav sidenav-expanded-lg');
    }

    if(document.getElementById("menu").getAttribute("aria-sidebarStateExpanded") == "true") {
        $("#menu").click();
    }

    $("#sidebar-toggle-button").attr("class","fa fa-angle-double-right");
    $("#hide-btn").attr("onclick","openNav()");
     
    document.getElementById("mySidenav").style.width = "56px";
    document.getElementById("main").style.marginLeft = "56px";

    toggleTextVisibility("menu-text", "none");
    document.getElementById("hide-item").style.display = "none";

    $("#menu").attr("data-target","");
    $("#menu").toggleClass('collapse');
}

function openSidebarAbove() {

    if($("#mySidenav").hasClass("sidenav")) {
        $("#mySidenav").toggleClass('sidenav sidenav-expanded-md');
    }

    $("#menu").attr("data-target","#menu1");

    document.getElementById("mySidenav").style.width = "300px";
    document.getElementById("main").style.marginLeft = "300px";

    toggleTextVisibility("menu-text", "inline");
    document.getElementById("hide-item").style.display = "inline";

    document.getElementById("overlay").style.display = "inline";
}

function closeSidebarAbove() {

	document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";

    toggleTextVisibility("menu-text", "none");
    document.getElementById("menu").style.display = "none";
    document.getElementById("hide-item").style.display = "none";

    document.getElementById("overlay").style.display = "none";

    if(document.getElementById("menu1").getAttribute("aria-sidebarStateExpanded") == "true") {
        $("#menu").click();
    }
}

function closeMenuOnLittleScreens() {
    if($(window).width() < 992) {
        closeSidebarAbove();
    }
}

$(window).resize(function resize() {
    if($(window).width() >= 992) {
       
        if($("#mySidenav").hasClass("sidenav-expanded-md")) {
            $("#mySidenav").toggleClass('sidenav-expanded-lg sidenav-expanded-md');
            $("#sidebar-toggle-button").attr("class","fa-angle-double-left");

            document.getElementById("mySidenav").style.width = "300px";
            document.getElementById("main").style.marginLeft = "300px";

            toggleTextVisibility("menu-text", "inline");
            document.getElementById("hide-item").style.display = "inline";
            
            $("#sidebar-toggle-button").attr("onclick","closeNav()");
            $("#sidebar-toggle-button").attr("class","fa-angle-double-left");
            $("#menu").attr("data-target","#menu1");

            document.getElementById("overlay").style.display = "none";
        }

    } else if($(window).width() < 992) {
        if($("#mySidenav").hasClass("sidenav-expanded-lg")) {
           $("#mySidenav").toggleClass('sidenav-expanded-lg sidenav');
    }
}
});

function toggleTextVisibility(className, visible) {
    elements = document.getElementsByClassName(className);
    for (var i = 0; i < elements.length; i++)
        elements[i].style.display = visible;
}

function workAreaMargin() {
    $("#work-area").toggleClass('work-area-margin');
}

function underline(tabName) {

    if(!$(tabName).hasClass("underline-tab")) {
        $("#tab-text1").toggleClass('underline-tab');
        $("#tab-text2").toggleClass('underline-tab');
    }  
}

function onCandidateClick() {

    if(document.getElementById("candidates-block").style.display == "none")
        document.getElementById("candidates-block").style.display = "inline";
    else
        document.getElementById("candidates-block").style.display = "none";


    if(document.getElementById("info-area").style.display == "none") {
       if($(window).width() > 991)
            document.getElementById("info-area").style.display = "flex";
        else
            document.getElementById("info-area").style.display = "inline";
    }
    else
        document.getElementById("info-area").style.display = "none";

    
    if(document.getElementById("close-info-area").style.display == "none")
        document.getElementById("close-info-area").style.display = "inline";
    else
        document.getElementById("close-info-area").style.display = "none";


    if(document.getElementById("openSidebarAboveButton").style.display == "none")
        document.getElementById("openSidebarAboveButton").style.display = "flex";
    else
        document.getElementById("openSidebarAboveButton").style.display = "none";
}