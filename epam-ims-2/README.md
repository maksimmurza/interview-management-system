# Interview management system (Epam internal project)

#### Description

Application allows to collect useful information about candidates 
for different positions in EPAM and to provide more 
effective selection of workers.

![Imgur](http://i.imgur.com/HPtbl5p.png)

#### Modules:

* **ims-rest** - REST service to receive data
* **ims-web** - Angular based web-application
* **html-template** - prototype of application
* **json-server** - server with fake data for testing
* **docs** - specifications

#### Dependencies:

* [NodeJs](https://nodejs.org/en/), [npm](https://www.npmjs.com/):
    - Ubuntu: \
    `# apt-get install nodejs` \
    `# apt-get install npm`
    - Arch-based distros: \
    `# pacman -S nodejs npm`
    
* [Angular](https://angular.io/): \
        `# npm install -g @angular/cli`

* [Json-server](https://github.com/typicode/json-server): \
        `# npm install -g json-server`
        
#### Run:

* Run server with fake data:

        $ cd ims/json-server
        $ npm install faker lodash
        $ json-server generate.js

* Check generating: \
[http://localhost:3000](http://localhost:3000) \
[http://localhost:3000/candidates](http://localhost:3000/candidates)

* Run web-application:

        $ cd ims/ims-web
        $ npm install
        $ ng serve --open

App will open (automatically with help of `--open`) on address [http://localhost:4200](http://localhost:4200)